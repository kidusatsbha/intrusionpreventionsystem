
package jpcaptsting;

import javax.swing.JOptionPane;
import jpcap.*;
import javax.swing.JOptionPane;


public class udpPacketAnalser extends javax.swing.JFrame {
Packet_Thread CATN;
    NetworkInterface[]interfaces;
     JpcapCaptor captor;
int index=0,counter=0;
boolean captureState=false;
   
    public udpPacketAnalser() {
        initComponents();
    }

    public void capture(){
     
     CATN=new Packet_Thread(){
         
         public Object construct(){
             jTextArea1.setText("new capture at interface"+"  "+index+
                     "\n\n------------------------------------------------------------------------"
                     +"----------------------------------------------------------------\n\n");
             try{
                 captor=JpcapCaptor.openDevice(interfaces[index],65535,false,20);
                 while(captureState){
                     captor.processPacket(1, new  Printer());
                     captor.setFilter("arp ", true);
                  
                   
                 }
               captor.close();
         }
             catch(Exception e){
               JOptionPane.showMessageDialog(null, e);
             }return 0;
}
         public void fnished(){
             this.interrupt();
         }
     };
             
     
    CATN.start();
 }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        List = new javax.swing.JButton();
        interfaceN0 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        selectInterface = new javax.swing.JButton();
        capture = new javax.swing.JButton();
        Stop = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("udp packet analyser");

        List.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        List.setText("List nterface");
        List.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ListActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        selectInterface.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        selectInterface.setText("select Interface");
        selectInterface.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectInterfaceActionPerformed(evt);
            }
        });

        capture.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        capture.setText("Capture");
        capture.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                captureActionPerformed(evt);
            }
        });

        Stop.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        Stop.setText("Stop");
        Stop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                StopActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(186, 186, 186)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(184, 184, 184)
                                .addComponent(interfaceN0, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(List)
                                .addGap(18, 18, 18)
                                .addComponent(selectInterface)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(capture)))
                        .addGap(14, 14, 14)
                        .addComponent(Stop))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 946, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(40, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(interfaceN0, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(List)
                    .addComponent(selectInterface)
                    .addComponent(capture)
                    .addComponent(Stop))
                .addGap(199, 199, 199))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>                        

    private void ListActionPerformed(java.awt.event.ActionEvent evt) {                                     
       //List network interfaces
        interfaces=JpcapCaptor.getDeviceList();
        //for each network interface
for (int i = 0; i <interfaces.length; i++) {
    counter++;
    //print out its name and description
    jTextArea1.append(i+": "+interfaces[i].name + "(" +interfaces[i].description+")");
  
 

 

  //print out its MAC address
 
  jTextArea1.append(" MAC address:");
  for (byte b :interfaces[i].mac_address)
    //System.out.print(Integer.toHexString(b&0xff) + ":");
  jTextArea1.append(Integer.toHexString(b&0xff) + ":");
 

  //print out its IP address, subnet mask and broadcast address
  for (NetworkInterfaceAddress a : interfaces[i].addresses)
  
  jTextArea1.append("\n Ip address:"+a.address + "\n subnet mask " + a.subnet + "\n Broadcast address "+ a.broadcast);
}
    }                                    

    private void selectInterfaceActionPerformed(java.awt.event.ActionEvent evt) {                                                
         //select interface 
        int tempo=Integer.parseInt(interfaceN0.getText());
        if(tempo>-1&&tempo<counter){
            index=tempo;
            
        }
        else{
            JOptionPane.showMessageDialog(null,"index out of range index#0 to  "+(counter-1)+"is allowed");
        }
       interfaceN0.setText("");
    }                                               

    private void captureActionPerformed(java.awt.event.ActionEvent evt) {                                        
      captureState=true;
        capture();
    }                                       

    private void StopActionPerformed(java.awt.event.ActionEvent evt) {                                     
        // TODO add your handling code here:
    }                                    

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(udpPacketAnalser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(udpPacketAnalser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(udpPacketAnalser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(udpPacketAnalser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new udpPacketAnalser().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton List;
    private javax.swing.JButton Stop;
    private javax.swing.JButton capture;
    private javax.swing.JTextField interfaceN0;
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTextArea jTextArea1;
    private javax.swing.JButton selectInterface;
    // End of variables declaration                   
}
